#!/usr/bin/env bash

TMP="$(mktemp)"
curl -sSL https://gitlab.com/megabyte-labs/git-sync/-/raw/master/gomplate.yml > "$TMP"

if ! command -v brew; then
    bash <(curl -sSL https://install.doctor/brew)
fi

if ! command -v gomplate; then
    brew install gomplate
fi

if command -v gomplate; then
    gomplate --config "$TMP"
fi
